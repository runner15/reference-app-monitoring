/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for calling org APIs
*/


/* Get a list of orgs and shows them in a listbox for selection
*/

function getOrgsForSelection() {
    //console.log('getting orgs for: ' + APIUSERNAME);
    $('#divSelectOrg').find('#select-org-wait').show();

    $.ajax({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {

            // Create an html option for each map entry found.
            var items = $.map(result, function (orgItem, index) {
                var optionItem = $('<option value="' + orgItem.id + '">' + orgItem.name + '</option>')
                return optionItem;
            });

            var selectOrg = $('#selectOrg');
            selectOrg.append(items);


            // Show the list of orgs
            $('#divSelectOrg').show();
            $('#divSelectOrg').find('#divSelectOrgList').html(selectOrg);
            $('#divSelectOrg').find('#divSelectOrgList').show();
            $('#divSelectOrg').find('#select-org-wait').hide();

            

            // Wire up the click handler for this list box that is showing the orgs to show the solutions for the select org
             $('#divSelectOrg').find('#selectOrg').on('change', function () {
                getSolutionsForOrg();         
             }); 

            //Select the First Item in the list -- 
            $('#selectOrg').prop('selectedIndex', 0); 
            $('#selectOrg').trigger('change'); 


        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            $('#' + containerId).find('#select-org-wait').hide();

            var responseInfo = $('<p>Error getting the orgs for: ' + APIUSERNAME + ' - ' + errorThrown + '</p>');
            var divFailure = $('#' + containerId).find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}




